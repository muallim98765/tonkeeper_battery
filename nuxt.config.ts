// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      title: "Батарея Тонкипера",
      link: [{ rel: 'icon', type: 'image/svg+xml', href: '/logo.svg' }],
    },
  },
  devtools: { enabled: true },
  modules: [
    '@ant-design-vue/nuxt',
    'nuxt-svgo',
  ],
  css: ['@/assets/fonts/product-sans/stylesheet.css', '@/assets/scss/main.scss'],
  svgo: {
    defaultImport: 'component',
    componentPrefix: 'svg',
    svgoConfig: {
      plugins: [
        {
          name: 'preset-default',
          params: {
            overrides: {
              inlineStyles: {
                onlyMatchedOnce: false
              },
              // or disable plugins
              removeDoctype: false,
              removeViewBox: false
            }
          }
        }
      ]
    }
  },
})
