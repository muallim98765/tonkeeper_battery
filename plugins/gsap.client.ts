import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'
import {MotionPathPlugin  } from 'gsap/MotionPathPlugin'
import { Draggable } from 'gsap/Draggable'

// @ts-ignore
export default defineNuxtPlugin(() => {
  if (import.meta.client) {
    gsap.registerPlugin(ScrollTrigger, ScrollToPlugin, Draggable, MotionPathPlugin )
  }

  return {
    provide: {
      gsap,
      Draggable,
      ScrollTrigger,
    },
  }
})